package cz.muni.fi.pa165.model.dto.company;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyDto extends DomainObjectDto {
	private String name;
}
