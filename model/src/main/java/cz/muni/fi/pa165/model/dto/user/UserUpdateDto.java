package cz.muni.fi.pa165.model.dto.user;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserUpdateDto extends DomainObjectDto {
	private String username;

	private String password;

	private String email;

	private String firstName;

	private String lastName;
}
