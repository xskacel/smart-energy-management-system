package cz.muni.fi.pa165.model.dto.role;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto extends DomainObjectDto {
    protected String userId;
    protected RoleTypeEnum roleType;
}
