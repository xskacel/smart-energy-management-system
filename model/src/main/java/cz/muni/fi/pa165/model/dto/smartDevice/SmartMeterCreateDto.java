package cz.muni.fi.pa165.model.dto.smartDevice;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import cz.muni.fi.pa165.model.dto.device.DeviceDto;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SmartMeterCreateDto extends DomainObjectDto {
  private String name;
  private String houseId;
  private String deviceId;
}
