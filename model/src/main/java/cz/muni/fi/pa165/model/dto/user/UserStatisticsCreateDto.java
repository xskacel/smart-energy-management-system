package cz.muni.fi.pa165.model.dto.user;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserStatisticsCreateDto {

    private String userId;
    private String houseId;
    private Instant startTime;
    private Instant endTime;
}
