package cz.muni.fi.pa165.model.dto.metrics;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetricsUpdateDto extends DomainObjectDto {
    private double consumptionKWH;
}
