package cz.muni.fi.pa165.model.dto.house;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseUpdateDto extends DomainObjectDto {
	private String name;
	private String city;
	private String state;
	private String zipcode;
}
