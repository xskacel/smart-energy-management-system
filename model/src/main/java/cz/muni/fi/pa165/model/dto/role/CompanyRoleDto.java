package cz.muni.fi.pa165.model.dto.role;

import cz.muni.fi.pa165.model.dto.role.enums.CompanyRoleEnum;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyRoleDto extends RoleDto{
    private String CompanyId;
    private CompanyRoleEnum companyRole;
    private RoleTypeEnum roleType = RoleTypeEnum.CompanyRole;
}
