package cz.muni.fi.pa165.model.dto.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginInfoDto {
	private String username;
	private String password;
}
