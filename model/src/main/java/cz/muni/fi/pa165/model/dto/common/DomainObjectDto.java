package cz.muni.fi.pa165.model.dto.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DomainObjectDto {
  private String id;
}
