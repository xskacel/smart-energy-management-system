package cz.muni.fi.pa165.model.dto.house;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseCreateDto extends DomainObjectDto {

  private String address;
  private String city;
  private String state;
  private String zipcode;
}
