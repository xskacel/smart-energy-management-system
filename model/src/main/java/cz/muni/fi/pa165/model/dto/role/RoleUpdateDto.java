package cz.muni.fi.pa165.model.dto.role;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;

public class RoleUpdateDto extends DomainObjectDto {
    RoleTypeEnum roleType;
    String userId;
}
