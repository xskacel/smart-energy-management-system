package cz.muni.fi.pa165.model.dto.role;

import cz.muni.fi.pa165.model.dto.role.enums.HouseRoleEnum;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HouseRoleDto extends RoleDto{
    private String houseId;
    private HouseRoleEnum houseRole;
    private RoleTypeEnum roleType = RoleTypeEnum.HouseRole;
}
