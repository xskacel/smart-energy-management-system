package cz.muni.fi.pa165.model.dto.role.enums;

public enum RoleTypeEnum {
    Admin,
    HouseRole,
    CompanyRole,
}
