package cz.muni.fi.pa165.model.dto.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePasswordDto {
	private String oldPassword;
	private String newPassword;
	private String newPasswordConfirmation;
}
