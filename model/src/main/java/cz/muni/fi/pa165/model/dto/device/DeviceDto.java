package cz.muni.fi.pa165.model.dto.device;

import cz.muni.fi.pa165.model.dto.common.DomainObjectDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceDto extends DomainObjectDto {

  private String name;
}
