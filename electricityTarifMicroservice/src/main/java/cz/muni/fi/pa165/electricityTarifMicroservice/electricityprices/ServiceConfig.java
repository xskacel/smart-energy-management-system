package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class ServiceConfig {
}
