package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

import cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos.ElectricityPriceGetFullDto;
import cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos.ElectricityPricePostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ElectricityPriceService {
  double averagePriceLowTariff();

  public double averagePriceHighTariff();

  ElectricityPriceGetFullDto getElectricityPriceObj(String companyId);
  double getElectricityPrice(String companyId); // dependent on time - low or high tariff

  Page<ElectricityPriceGetFullDto> getAllElectricityPricePagable(Pageable pages);

  List<ElectricityPriceGetFullDto> getAllElectricityPrice();

  String deleteElectricityPriceForCompany(String companyId);

    ElectricityPrice setElectricityPrice(ElectricityPricePostDto req);
}
