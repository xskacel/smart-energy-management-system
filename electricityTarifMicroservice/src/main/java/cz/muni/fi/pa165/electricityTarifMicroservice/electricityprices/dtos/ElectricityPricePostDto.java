package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ElectricityPricePostDto {

    @NotNull
    @JsonProperty("companyId")
    private String companyId;

    @NotNull
    @JsonProperty("priceHighTariff")
    private double priceHighTariff;

    @NotNull
    @JsonProperty("priceLowTariff")
    private double priceLowTariff;
}
