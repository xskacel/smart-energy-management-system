package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ElectricityPriceGetSlimDto {
    private Long id;

    private String companyId;

    private double price;
}
