package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

public class NoElectricityPriceForCompanyException extends RuntimeException {
    public NoElectricityPriceForCompanyException() {
    }

    public NoElectricityPriceForCompanyException(String message) {
        super(message);
    }

    public NoElectricityPriceForCompanyException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoElectricityPriceForCompanyException(Throwable cause) {
        super(cause);
    }

    public NoElectricityPriceForCompanyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
