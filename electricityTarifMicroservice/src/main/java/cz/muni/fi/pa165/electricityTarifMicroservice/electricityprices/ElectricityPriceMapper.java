package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

import cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos.ElectricityPriceGetFullDto;
import cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices.dtos.ElectricityPricePostDto;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ElectricityPriceMapper {
    @BeforeMapping()
    void addId() {

    }

    abstract List<ElectricityPriceGetFullDto> electricityPriceListToElectricityPriceGetFullDtoList(List<ElectricityPrice> electricityPriceList);

    abstract ElectricityPrice electricityPricePostDtoToElectricityPrice(ElectricityPricePostDto electricityPricePostDto);

    abstract ElectricityPriceGetFullDto electricityPriceToElectricityPriceGetFullDto(ElectricityPrice electricityPrice);

    Page<ElectricityPriceGetFullDto> mapToPageDto(Page<ElectricityPrice> prices) {
        return new PageImpl<>(electricityPriceListToElectricityPriceGetFullDtoList(prices.getContent()), prices.getPageable(), prices.getTotalPages());
    }
}
