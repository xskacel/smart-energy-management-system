package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "electricityprices")
public class ElectricityPrice implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Override
  public String toString() {
    return "ElectricityPrice{" +
            "id='" + id + '\'' +
            ", companyId='" + companyId + '\'' +
            ", priceHighTariff=" + priceHighTariff +
            ", priceLowTariff=" + priceLowTariff +
            '}';
  }

  private String companyId;

  private double priceHighTariff;

  private double priceLowTariff;
}
