package cz.muni.fi.pa165.electricityTarifMicroservice.electricityprices;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ElectricityPriceRepository extends JpaRepository<ElectricityPrice, Long> {
    ElectricityPrice findByCompanyId(String companyId);

    @Modifying
    @Query(value = "DELETE FROM electricityprices WHERE company_id = ?1", nativeQuery = true)
    void deleteCompany(String companyId);

    @Query(value = "SELECT AVG(price_low_tariff) FROM electricityprices", nativeQuery = true)
    double averagePriceLowTariff();

    @Query(value = "SELECT AVG(price_high_tariff) FROM electricityprices", nativeQuery = true)
    double averagePriceHighTariff();
}
