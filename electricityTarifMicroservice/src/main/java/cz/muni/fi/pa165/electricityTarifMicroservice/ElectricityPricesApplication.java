package cz.muni.fi.pa165.electricityTarifMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectricityPricesApplication {
  public static void main(String[] args) {
    SpringApplication.run(ElectricityPricesApplication.class, args);
  }
}
