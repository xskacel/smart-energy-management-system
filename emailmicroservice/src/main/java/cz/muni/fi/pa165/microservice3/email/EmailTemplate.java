package cz.muni.fi.pa165.microservice3.email;

import jakarta.validation.constraints.Email;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class EmailTemplate {
	@Email
	private String sendTo;

	private String subject;

	private String body;

}
