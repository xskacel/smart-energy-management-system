package cz.muni.fi.pa165.microservice3.email;

import java.util.regex.Pattern;

public class EmailUtils {

	private static final Pattern EMAIL_PATTERN = Pattern.compile(
			"^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
	);

	public static boolean isValidEmail(String email) {
		return EMAIL_PATTERN.matcher(email).matches();
	}

	public static final String EMAIL_TEMPLATE = """
			Hello,

			We are sending you the daily energy consumption statistics for your house(s).

			{houseStatistics}
			Best regards,
			Your Smart Energy Management System""";

	public static final String HOUSE_STATISTICS_TEMPLATE = """
			The daily energy consumption statistics for house {houseAddress}:

			{statistics} kWh

			""";
}
