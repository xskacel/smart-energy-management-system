package cz.muni.fi.pa165.microservice3.email;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmailControllerTest {

	@Autowired
	private MockMvc mockMvc;

	// injected mapper for converting classes into JSON strings
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void badRequestWithInvalidAddress() throws Exception {
		EmailTemplate emailTemplate = EmailTemplate
				.builder()
				.sendTo("email")
				.subject("Test")
				.body("Hello")
				.build();
		mockMvc
				.perform(post("/api/email/send")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(emailTemplate)))
				.andExpect(status().is4xxClientError());
	}

	@Test
	void sendEmailToValidAddress() throws Exception {
		EmailTemplate emailTemplate = EmailTemplate
				.builder()
				.sendTo("test@gmail.com")
				.subject("Test")
				.body("Hello")
				.build();
		mockMvc
				.perform(post("/api/email/send")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(emailTemplate)))
				.andExpect(status().is4xxClientError());
	}

}