package cz.muni.fi.pa165.statistics.statistics;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StatisticsRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private StatisticRepository statisticRepository;

    @Test
    public void createTesting() throws Exception {
        String userStatistics = this.entityManager.persistAndGetId(new UserStatistic()).toString();
        // Regular expression to match UUID format
        Pattern uuidPattern = Pattern.compile(
                "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}",
                Pattern.CASE_INSENSITIVE);
        // Check if the string matches the UUID format
        boolean isUUID = uuidPattern.matcher(userStatistics).matches();
        assertThat(isUUID).isTrue();
    }

    @Test
    public void shouldFindNoneIfRepositoryIsEmpty() {
        Iterable<UserStatistic> userStatistics = statisticRepository.findAll();
        assertThat(userStatistics).isEmpty();
    }

    @Test
    public void shouldStore() {
        assertThat(new UserStatistic()).hasFieldOrProperty("id");
    }

    @Test
    public void shouldFindAll() {
        UserStatistic m = new UserStatistic();
        entityManager.persist(m);

        UserStatistic m1 = new UserStatistic();
        entityManager.persist(m1);

        UserStatistic m2 = new UserStatistic();
        entityManager.persist(m2);

        assertThat(statisticRepository.findAll()).hasSize(3).contains(m, m1, m2);
    }

    @Test
    public void shouldUpdateById() {
        UserStatistic m = new UserStatistic();
        entityManager.persist(m);

        UserStatistic m1 = new UserStatistic();
        entityManager.persist(m1);

        UserStatistic update = statisticRepository.findById(m.getId()).get();
        update.setName("New name");
        statisticRepository.save(update);

        UserStatistic check = statisticRepository.findById(m.getId()).get();

        assertThat(check.getId()).isEqualTo(m.getId());
        assertThat(check.getName()).isEqualTo(m.getName());
    }

    @Test
    public void shouldDeleteById() {
        UserStatistic m = new UserStatistic();
        entityManager.persist(m);

        UserStatistic m1 = new UserStatistic();
        entityManager.persist(m1);

        statisticRepository.deleteById(m.getId());

        assertThat(statisticRepository.findAll()).hasSize(1).contains(m1);
        assertThat(statisticRepository.findAll()).hasSize(1).doesNotContain(m);
    }

    @Test
    public void shouldDeleteAll() {
        entityManager.persist(new UserStatistic());
        entityManager.persist(new UserStatistic());

        statisticRepository.deleteAll();

        assertThat(statisticRepository.findAll()).isEmpty();
    }
}
