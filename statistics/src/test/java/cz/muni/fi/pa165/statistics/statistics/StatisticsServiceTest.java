package cz.muni.fi.pa165.statistics.statistics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class StatisticsServiceTest {

    @InjectMocks
    private StatisticService statisticService;

    @Mock
    private StatisticRepository statisticRepository;

    @BeforeEach
    void init() { openMocks(this); }

    private static final String userIdTest = "1";

    @Test
    void shouldGetAllUserStatisticsForUser() {
        UserStatistic userStatisticForHouse1ForMonth1 = UserStatistic.builder().userId(userIdTest).userEmail("user123@gmail.com").houseId("1").monthNum(1).monthlyConsumption(45000.0).build();
        UserStatistic userStatisticForHouse2ForMonth1 = UserStatistic.builder().userId(userIdTest).userEmail("user123@gmail.com").houseId("2").monthNum(1).monthlyConsumption(45000.0).build();
        List<UserStatistic> userStatisticList = List.of(userStatisticForHouse1ForMonth1, userStatisticForHouse2ForMonth1);
        when(statisticRepository.findAllByUser(userStatisticForHouse1ForMonth1.getUserId())).thenReturn(userStatisticList);

        List<UserStatistic> userStatistics = statisticService.findAllUserStatistics(userIdTest);

        assertEquals(userStatisticList, userStatistics);
    }
    @Test
    void shouldGetUserStatisticByUserId() {
        UserStatistic userStatistic = UserStatistic.builder().userId(userIdTest).userEmail("user123@gmail.com").houseId("1").monthNum(154).build();

        when(statisticRepository.save(userStatistic)).thenReturn(userStatistic);
    }

    @Test
    void shouldFindAllUserStatisticsByHouseEmpty() {
        List<UserStatistic> allUserStatisticsByHouse = statisticService.findAllUserStatisticsByHouse("1", "1");
        assertEquals(0, allUserStatisticsByHouse.size());
    }

}
