package cz.muni.fi.pa165.statistics.statistics;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserStatisticsMapper{
    UserStatistic fromDto(StatisticCreateDto entity);
    UserStatisticsDto toDto(UserStatistic entity);

    List<UserStatisticsDto> toDtoList(List<UserStatistic> entities);
}
