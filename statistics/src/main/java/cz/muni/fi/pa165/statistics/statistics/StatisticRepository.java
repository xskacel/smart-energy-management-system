package cz.muni.fi.pa165.statistics.statistics;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatisticRepository extends JpaRepository<UserStatistic, String> {

    @Query("select new UserStatistic (u.id, u.userId, u.userEmail, u.houseId, u.monthNum, u.yearNum, u.monthlyConsumption, u.name)  from UserStatistic u where u.userId = :#{#userId}")
    List<UserStatistic> findAllByUser(@Param("userId") String userId);

    @Query("select new UserStatistic (u.id, u.userId, u.userEmail, u.houseId, u.monthNum, u.yearNum, u.monthlyConsumption, u.name)  from UserStatistic u where u.userId = :#{#userId} and u.houseId = :#{#houseId}")
    List<UserStatistic> findAllByUserAndHouse(@Param("userId") String userId, @Param("houseId") String houseId);

    @Query("select new UserStatistic(u.id, u.userId, u.userEmail, u.houseId, u.monthNum, u.yearNum, u.monthlyConsumption, u.name)  from UserStatistic u where u.userId = :#{#userId} and u.houseId = :#{#houseId} and u.monthNum = :#{#month} and u.yearNum = :#{#year}")
    UserStatistic findUserHouseDateStatistics(@Param("userId") String userId, @Param("houseId") String houseId, @Param("month") int month, @Param("year") int year);
}
