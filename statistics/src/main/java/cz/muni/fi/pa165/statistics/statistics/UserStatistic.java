package cz.muni.fi.pa165.statistics.statistics;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_userStatistics")
public class UserStatistic {
    @Id
    private String id = UUID.randomUUID().toString();
    private String userId;
    private String userEmail;
    private String houseId;
    private int monthNum;
    private int yearNum;
    private Double monthlyConsumption;
    private String name;
}
