package cz.muni.fi.pa165.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StatisticsApplication {

  public static void main(String[] args) {
    SpringApplication.run(StatisticsApplication.class, args);
  }
}
