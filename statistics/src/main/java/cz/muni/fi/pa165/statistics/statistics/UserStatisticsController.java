package cz.muni.fi.pa165.statistics.statistics;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/user-statistics")
@Tag(name = "Statistics", description = "Statistics API")
public class UserStatisticsController {

    private final StatisticsFacade statisticsFacade;

    @Autowired
    public UserStatisticsController(StatisticsFacade statisticsFacade)
    {
        this.statisticsFacade = statisticsFacade;
    }

    @Operation(
            summary = "Create statistics",
            description = "Creates a new statistics.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Statistics created successfully")
            }
    )
    @PostMapping
    public UserStatisticsDto create(@RequestBody @Valid StatisticCreateDto createDto) throws IOException {
        return statisticsFacade.CreateStatistics(createDto);
    }

    @Operation(
            summary = "Get user statistics",
            description = "Get user statistics.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Statistics created successfully"),
                    @ApiResponse(responseCode = "404", description = "User not found")
            }
    )
    @GetMapping("/all")
    public List<UserStatisticsDto> findAllByUser(@RequestParam @Valid String userId) {
        return statisticsFacade.FindAllByUser(userId);
    }

    @Operation(
            summary = "Get user statistics for house",
            description = "Get user statistics for house.")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Statistics created successfully"),
                    @ApiResponse(responseCode = "404", description = "User/House not found")
            }
    )
    @GetMapping("/user")
    public List<UserStatisticsDto> findHouseStatisticsByUser(@RequestParam @Valid String userId,
                                                         @RequestParam @Valid String houseId) {
        return statisticsFacade.findHouseStatisticsByUser(userId, houseId);
    }
}
