package cz.muni.fi.pa165.statistics.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Component
@Transactional
public class StatisticsFacade {

    private final StatisticService statisticService;

    private final UserStatisticsMapper mapper;
    @Autowired
    public StatisticsFacade(StatisticService statisticService, UserStatisticsMapper mapper) {
        this.statisticService = statisticService;
        this.mapper = mapper;
    }

    @Transactional
    public UserStatisticsDto CreateStatistics(StatisticCreateDto entity) throws IOException {
        return mapper.toDto(statisticService.createStatistic(mapper.fromDto(entity)));
    }

    @Transactional
    public List<UserStatisticsDto> FindAllByUser(String userId){
        return mapper.toDtoList(statisticService.findAllUserStatistics(userId));
    }

    @Transactional
    public List<UserStatisticsDto> findHouseStatisticsByUser(String userId,
                                                             String houseId) {
        return mapper.toDtoList(statisticService.findAllUserStatisticsByHouse(userId, houseId));
    }
}
