package cz.muni.fi.pa165.statistics.statistics;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.Date;

@Setter
@Getter
@Builder
@Data
public class StatisticCreateDto {
    private String userId;
    @Email
    private String userEmail;
    private String houseId;
    @Min(1)
    @Max(12)
    private int monthNum;
    @Min(2000)
    // TODO: replace with current year
    @Max(2030)
    private int yearNum;
}
