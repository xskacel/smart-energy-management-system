# Smart Energy Management System

## About project

- **Name**: Smart Energy Management System
- **Developers**:
    - Marek Skácelík - _Project Leader_ @xskacel
    - Anesa Fazlagić @xfazlag
    - Filip Bugoš @xbugos
    - Miroslav Rouča @xrouca
- **Assigment**:
    - The system will allow users to manage the smart meter devices at home. For a user, it will be possible to set different smart meters for the different houses they own/rent, and track the power consumption statistics over time that is collected automatically. They can track the power consumption of different devices in different time of the day (e.g., at night). They can also shut down the power to one of their houses through the system, when power is not needed. The power distribution company can consult the power consumption statistics over period of times of all the users registered into the system.

## Modules
  - **Core**
    - Main module
  - **Statistics**
    - Getting power consumption statistics
  - **Email**
    - Sending emails from the system
  - **Electricity Price**
    - Getting electricity price in current time (high or low tariff)

## Roles

System has three authorization roles - **Admin**, **Power Distribution Company Employee** and **Smart Energy User**.

- Admin has full permissions in the system.
- Power distribution company employee can add or modify smart devices for the company and get the houses' power consumption statistics.
- Smart energy users manage smart devices in their houses. They can add devices to the house and then get power consumption statistics. They can also shut down the power for the smart device in the house.

## Use Case diagram

![](images/UseCase.jpg)

## Class diagram

![](images/ClassDiagram.jpg)

## Docker

To run contanariazed application run the command in root folder:

    docker-compose up

End application with command:

	docker-compose down

After updates in code run command:

	docker-compose down --rmi all

Now you can run compose up with code updates :).
Service ports are not changed.


If you want to run core microservice locally, run postgres with adminer by following steps:

	- cd ./core
	- docker-compose up

If you want to run statistics microservice locally, run postgres with adminer by following steps:

	- cd ./core
	- docker-compose up

Adminer is a soft enabling developers to view postgres db.
It runs on port defined in docker compose file.
Sign up with adminer with this config.:

	system <postgresql>
	server: <name_of_the_container_db>
	username: <username_defined_in_docker_compose_file>
	password: <password_defined_in_docker_compose_file>
	db: (stays empty)

  ## Seeding/clearing data
  If you wish to seed some data into your database just uncomment `datainitializer` container in the `docker-compose.yaml` (if you wish to run the whole app with all of their microservices) or in the `core/src/main/resources/application.yml` (just for the core microservice).
  And to clear all the data from the database uncomment `dataclearer` container from the same files.

  ## Prometheus & Grafana:

  System provides statistics on start time, uptime, heap usage, load average etc., for each of the microservices.
  Prometheus scrapes exposed metrics by Micrometer.
  Grafana presents these data visually with custom dashboard.
  After running contanariazed application in the root folder, the microservices should be up and running, the Prometheus and Grafana containers as well.
  If they are created but have not started, you can open your Docker Desktop application and manually start the created Prometheus and Grafana containers.
  If you encounter the error ERR_EMPTY_RESPONSE in your browser, use Incognito mode.
  
  Prometheus is scraping the metrics of each microservice every 10 seconds.
  Prometheus is running on port 9090.

  Grafana is running on port 3000.
  Sign up to grafana with this config.:
  user: admin
  password: password

  Once you are logged in, choose Dashboards -> Browse -> Smart Energy Management System Statistics.
  You should see this dashboard (the read square depicts the place you browse metrics for particular microservice):
  
  ![](images/GrafanaDashboard.png)

 ## Locust stress testing:

  Python script for stress testing the system using locust library is in the locustScript folder.
  The script (locustfile.py) is written to show the performance of the electricity price microservice.
  Add a Python interpreter in IntelliJ IDEA and install package locust (or with the command pip3 install locust).
  You can stress test yourself (port 8090), from the locustScript folder in the terminal, with the command:

    locust

  The stress testing report is in the folder reports.
  After starting the test, the process should look like this:

  ![](images/locustTesting.png)

## OAuth:
To be able to call Smart Management System API one should be registered within the system. The registration could
be done with OAuth microservice on port 8082. Type url:8082/register and it will guide you to register with MUNI OIDC.
After that, the user account is created within our database and during every API call one have to provide valid Bearer access token
that is than validated with MUNI. The token should contains email of the user trying to use our API. When we have the user in the database, everything works correctly.

After registration the token will can be found either in terminal window or directly in browser.
