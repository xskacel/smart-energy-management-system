package cz.muni.fi.pa165.core;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;

public class Persistence {
	private static final EntityManagerFactory factory = jakarta.persistence.Persistence.createEntityManagerFactory("jpa.persistence.core");

	static EntityManager getEntityManager() {
		return factory.createEntityManager();
	}
}
