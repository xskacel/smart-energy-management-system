package cz.muni.fi.pa165.core.metrics;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.smartmeter.SmartMeter;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_metrics")
public class Metrics extends DomainObject {

    private double consumptionKWH;
    @ManyToOne
    private SmartMeter smartMeter;
}
