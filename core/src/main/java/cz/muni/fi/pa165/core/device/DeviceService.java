package cz.muni.fi.pa165.core.device;

import cz.muni.fi.pa165.core.common.DomainService;
import cz.muni.fi.pa165.core.helpers.exceptions.EntityDeletionException;
import cz.muni.fi.pa165.core.house.House;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class DeviceService extends DomainService<Device> {
  @Getter
  private final DeviceRepository repository;

  @Autowired
  public DeviceService(DeviceRepository deviceRepository) {
    repository = deviceRepository;
  }

  @Transactional(readOnly = true)
  public Device findByName(String name) {
    return getRepository()
            .findByName(name)
            .filter(entity -> entity.getDeletedAt() == null)
            .orElse(null);
  }
}
