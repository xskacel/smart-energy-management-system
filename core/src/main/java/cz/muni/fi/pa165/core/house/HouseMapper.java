package cz.muni.fi.pa165.core.house;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.house.HouseCreateDto;
import cz.muni.fi.pa165.model.dto.house.HouseDto;
import cz.muni.fi.pa165.model.dto.house.HouseUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface HouseMapper extends DomainMapper<House, HouseDto, HouseCreateDto, HouseUpdateDto> {}
