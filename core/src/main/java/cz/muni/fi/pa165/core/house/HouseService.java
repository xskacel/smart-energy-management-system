package cz.muni.fi.pa165.core.house;

import cz.muni.fi.pa165.core.common.DomainService;
import cz.muni.fi.pa165.core.helpers.exceptions.EntityDeletionException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class HouseService extends DomainService<House> {
  @Getter
  private final HouseRepository repository;

  @Autowired
  public HouseService(HouseRepository houseRepository) {
    repository = houseRepository;
  }
}
