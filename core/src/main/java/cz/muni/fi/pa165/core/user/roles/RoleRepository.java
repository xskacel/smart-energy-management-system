package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.core.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository<T extends Role> extends JpaRepository<T, String> {

    public List<T> findAllByUserId(String userId);
}
