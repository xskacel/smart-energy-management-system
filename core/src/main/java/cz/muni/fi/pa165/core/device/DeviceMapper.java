package cz.muni.fi.pa165.core.device;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.device.DeviceCreateDto;
import cz.muni.fi.pa165.model.dto.device.DeviceDto;
import cz.muni.fi.pa165.model.dto.device.DeviceUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface DeviceMapper extends DomainMapper<Device, DeviceDto, DeviceCreateDto, DeviceUpdateDto> { }
