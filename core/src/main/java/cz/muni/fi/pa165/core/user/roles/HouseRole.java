package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.core.house.House;
import cz.muni.fi.pa165.model.dto.role.enums.HouseRoleEnum;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_houseRoles")
public class HouseRole extends Role {

    @ManyToOne
    private House house;

    @Enumerated(EnumType.STRING)
    private HouseRoleEnum houseRole;
}
