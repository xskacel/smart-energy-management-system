package cz.muni.fi.pa165.core.metrics;

import cz.muni.fi.pa165.core.common.DomainFacade;
import cz.muni.fi.pa165.core.helpers.exceptions.EntityDeletionException;
import cz.muni.fi.pa165.core.smartmeter.SmartMeter;
import cz.muni.fi.pa165.core.smartmeter.SmartMeterService;
import cz.muni.fi.pa165.model.dto.metrics.MetricsCreateDto;
import cz.muni.fi.pa165.model.dto.metrics.MetricsDto;
import cz.muni.fi.pa165.model.dto.metrics.MetricsUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MetricsFacade extends DomainFacade<Metrics,
		MetricsDto,
		MetricsCreateDto,
		MetricsUpdateDto> {
	private MetricsService metricsService; // For the "MetricsService" specific methods
	private final SmartMeterService smartMeterService;
	@Autowired
	public MetricsFacade(MetricsService metricsService,
						 MetricsMapper metricsMapper, SmartMeterService smartMeterService) {
		super(metricsService, metricsMapper);
		this.metricsService = metricsService;
		this.smartMeterService = smartMeterService;
	}

	@Override
	public MetricsDto create(MetricsCreateDto metricsCreateDto){
		Metrics metrics = mapper.fromCreateDto(metricsCreateDto);
		SmartMeter smartMeter = smartMeterService.findById(metricsCreateDto.getSmartMeterId());
		metrics.setSmartMeter(smartMeter);

		Metrics metricsCreated = metricsService.create(metrics);
		return mapper.toDto(metricsCreated);
	}

	public void deleteMetricsById(String id) throws EntityDeletionException {
		metricsService.deleteById(id);
	}
}
