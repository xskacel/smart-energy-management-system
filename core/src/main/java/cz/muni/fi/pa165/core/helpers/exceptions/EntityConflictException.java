package cz.muni.fi.pa165.core.helpers.exceptions;

public class EntityConflictException extends RuntimeException {
	public EntityConflictException(String message) {
		super(message);
	}
}
