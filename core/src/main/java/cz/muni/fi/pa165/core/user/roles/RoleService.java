package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.core.common.DomainService;
import cz.muni.fi.pa165.core.helpers.exceptions.EntityDeletionException;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService<T extends Role> extends DomainService<T> {

    @Getter
    private final RoleRepository repository;

    @Autowired
    public RoleService(RoleRepository repository){
        this.repository = repository;
    }

    @Transactional
    public List<Role> findAllByUser(String id){
        return repository.findAllByUserId(id);
    }

    public Role deleteCompanyRole(String id) throws Exception {
        Optional<Role> role = repository.findById(id);

        if (role.get().roleType != RoleTypeEnum.CompanyRole){
            throw new Exception("ID does not match any company role.");
        }

        return super.deleteById(id);
    }
}
