package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.core.company.Company;
import cz.muni.fi.pa165.model.dto.role.enums.CompanyRoleEnum;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_companyRole")
public class CompanyRole extends Role{
    @ManyToOne
    private Company company;

    @Enumerated(EnumType.STRING)
    private CompanyRoleEnum companyRoleEnum;
}
