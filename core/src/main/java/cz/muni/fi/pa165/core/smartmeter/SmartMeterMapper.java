package cz.muni.fi.pa165.core.smartmeter;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.smartDevice.SmartMeterCreateDto;
import cz.muni.fi.pa165.model.dto.smartDevice.SmartMeterDto;
import cz.muni.fi.pa165.model.dto.smartDevice.SmartMeterUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface SmartMeterMapper extends DomainMapper<SmartMeter, SmartMeterDto, SmartMeterCreateDto, SmartMeterUpdateDto> {}

