package cz.muni.fi.pa165.core.user.roles;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@Table(name = "domain_adminRole")
public class AdminRole extends Role{
}
