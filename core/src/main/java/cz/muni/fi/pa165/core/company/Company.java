package cz.muni.fi.pa165.core.company;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.device.Device;
import cz.muni.fi.pa165.core.house.House;
import cz.muni.fi.pa165.core.user.roles.CompanyRole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_company")
public class Company extends DomainObject {
	@Column(unique = true, nullable = false)
	private String name;
	@OneToMany(mappedBy = "company")
	private List<CompanyRole> employeeList;

	@OneToMany(mappedBy = "company")
	private List<Device> deviceList;

	@OneToMany
	@JoinColumn(name = "domain_company_id")
	private List<House> houseList;
}
