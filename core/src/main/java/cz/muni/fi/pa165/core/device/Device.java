package cz.muni.fi.pa165.core.device;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.company.Company;
import cz.muni.fi.pa165.core.smartmeter.SmartMeter;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_device")
public class Device extends DomainObject {

  @Column(unique = true, nullable = false)
  private String name;

  @OneToMany(mappedBy = "device")
  private List<SmartMeter> smartMeterList;

  @ManyToOne
  private Company company;
}
