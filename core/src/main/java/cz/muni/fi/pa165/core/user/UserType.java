package cz.muni.fi.pa165.core.user;

public enum UserType {
  NORMAL,
  EMPLOYEE,
  ADMIN
}
