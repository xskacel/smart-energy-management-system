package cz.muni.fi.pa165.core.Jwt;

import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import org.springframework.security.core.context.SecurityContextHolder;

public final class AuthService {

    public static Boolean IsUserAdmin(){
        JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUser().getRolesList().stream().anyMatch(r -> r.getRoleType() == RoleTypeEnum.Admin);
    }

    public static Boolean HasUserHouseRoleOrAdmin(){
        JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUser().getRolesList().stream().anyMatch(r -> r.getRoleType() == RoleTypeEnum.Admin || r.getRoleType() == RoleTypeEnum.HouseRole);
    }

    public static Boolean HasUserCompanyRoleOrAdmin(){
        JwtUser user = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUser().getRolesList().stream().anyMatch(r -> r.getRoleType() == RoleTypeEnum.Admin || r.getRoleType() == RoleTypeEnum.CompanyRole);
    }
}
