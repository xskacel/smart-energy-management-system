package cz.muni.fi.pa165.core.smartmeter;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.device.Device;
import cz.muni.fi.pa165.core.house.House;
import cz.muni.fi.pa165.core.metrics.Metrics;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_smartMeter")
public class SmartMeter extends DomainObject {

    @Column(unique = true, nullable = false)
    private String name;

    private boolean active;

    @ManyToOne
    private Device device;

    @ManyToOne
    private House house;

    @OneToMany(mappedBy = "smartMeter")
    private List<Metrics> metricsList;
}
