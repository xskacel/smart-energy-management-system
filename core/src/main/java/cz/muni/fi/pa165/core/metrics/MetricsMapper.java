package cz.muni.fi.pa165.core.metrics;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.metrics.MetricsCreateDto;
import cz.muni.fi.pa165.model.dto.metrics.MetricsDto;
import cz.muni.fi.pa165.model.dto.metrics.MetricsUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface MetricsMapper extends DomainMapper<Metrics,
		MetricsDto,
		MetricsCreateDto,
		MetricsUpdateDto> {}
