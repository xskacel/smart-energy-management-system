package cz.muni.fi.pa165.core.house;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.smartmeter.SmartMeter;
import cz.muni.fi.pa165.core.user.roles.HouseRole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_house")
public class House extends DomainObject {

  private String address;
  private String city;
  private String state;
  private String zipcode;

  @OneToMany(mappedBy = "house")
  private List<SmartMeter> smartMeterList;

  @OneToMany(mappedBy = "house")
  private List<HouseRole> ownerList;
}
