package cz.muni.fi.pa165.core.company;

import cz.muni.fi.pa165.core.common.DomainService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompanyService extends DomainService<Company> {
	@Getter
	private final CompanyRepository repository;

	@Autowired
	public CompanyService(CompanyRepository companyRepository) {
		this.repository = companyRepository;
	}

	@Transactional(readOnly = true)
	public Company findByName(String name) {
		return getRepository()
				.findByName(name)
				.filter(entity -> entity.getDeletedAt() == null)
				.orElse(null);
	}
}


