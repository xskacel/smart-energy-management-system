package cz.muni.fi.pa165.core;

import cz.muni.fi.pa165.core.company.CompanyService;
import cz.muni.fi.pa165.core.device.DeviceService;
import cz.muni.fi.pa165.core.house.HouseService;
import cz.muni.fi.pa165.core.metrics.MetricsService;
import cz.muni.fi.pa165.core.smartmeter.SmartMeterService;
import cz.muni.fi.pa165.core.user.UserService;
import cz.muni.fi.pa165.core.user.roles.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * Class for clearing data of the core database.
 * To execute the clearer you need to have
 * <code>
 *     dataclearer:
 *          enabled: true
 * </code>
 * in the <code>application.yaml</code> or root <code>docker-compose.yaml</code>
 *
 * @author xskacel
 */
@RequiredArgsConstructor
@Component
@ConditionalOnProperty(name = "dataclearer.enabled")
public class DataClearer implements ApplicationRunner {

	private static final Logger LOG = Logger.getLogger(DataClearer.class.getName());


	private final UserService userService;
	private final DeviceService deviceService;
	private final SmartMeterService smartMeterService;
	private final MetricsService metricsService;
	private final HouseService houseService;
	private final RoleService roleService;
	private final CompanyService companyService;

	@Override
	public void run(ApplicationArguments args) {
		LOG.info("Initializing Data Clearer");
		metricsClear();
		smartMeterClear();
		deviceClear();
		roleClear();
		companyClear();
		houseClear();
		userClear();
		LOG.info("Data Clearer Finished");
	}

	private void deviceClear() {
		LOG.info("Deleting Device Data");
		deviceService.hardDeleteAll();
	}

	private void smartMeterClear() {
		LOG.info("Deleting SmartMeter Data");
		smartMeterService.hardDeleteAll();
	}

	private void metricsClear() {
		LOG.info("Deleting Metrics Data");

		metricsService.hardDeleteAll();
	}

	private void houseClear() {
		LOG.info("Deleting House Data");
		houseService.hardDeleteAll();
	}

	private void roleClear() {
		LOG.info("Deleting Role Data");
		roleService.hardDeleteAll();
	}
	private void companyClear() {
		LOG.info("Deleting Company Data");
		companyService.hardDeleteAll();
	}
	private void userClear() {
		LOG.info("Deleting User Data");
		userService.hardDeleteAll();
	}

}
