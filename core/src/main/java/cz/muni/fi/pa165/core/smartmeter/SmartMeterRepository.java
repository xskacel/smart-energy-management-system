package cz.muni.fi.pa165.core.smartmeter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SmartMeterRepository extends JpaRepository<SmartMeter, String> {
	Optional<SmartMeter> findByName(String name);
}
