package cz.muni.fi.pa165.core.user.utils;

public class PasswordUtil {
	public static boolean  isPasswordValid(String password) {
		// check for minimum length of 8 characters
		if (password.length() < 8) {
			return false;
		}

		// check for white spaces
		if (password.chars().anyMatch(Character::isWhitespace)) {
			return false;
		}

		// check for at least one uppercase letter
		if (!password.matches(".*[A-Z].*")) {
			return false;
		}

		// check for at least one lowercase letter
		if (!password.matches(".*[a-z].*")) {
			return false;
		}
		return true;
	}
}
