package cz.muni.fi.pa165.core.company;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.company.CompanyCreateDto;
import cz.muni.fi.pa165.model.dto.company.CompanyDto;
import cz.muni.fi.pa165.model.dto.company.CompanyUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface CompanyMapper extends DomainMapper<Company,
		CompanyDto,
		CompanyCreateDto,
		CompanyUpdateDto>
{}
