package cz.muni.fi.pa165.core.metrics;

import cz.muni.fi.pa165.core.common.DomainService;
import lombok.Getter;
import org.springframework.stereotype.Service;

@Service
public class MetricsService extends DomainService<Metrics> {

    @Getter
    private final MetricsRepository repository;

    public MetricsService(MetricsRepository repository) {
        this.repository = repository;
    }

}
