package cz.muni.fi.pa165.core.user;

import cz.muni.fi.pa165.core.common.DomainMapper;
import cz.muni.fi.pa165.model.dto.user.UserCreateDto;
import cz.muni.fi.pa165.model.dto.user.UserDto;
import cz.muni.fi.pa165.model.dto.user.UserStatisticsCreateDto;
import cz.muni.fi.pa165.model.dto.user.UserUpdateDto;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper extends DomainMapper<
        User,
        UserDto,
        UserCreateDto,
        UserUpdateDto
        > {
  UserStatisticsCreateDto toStatisticsDto(User entity);
}
