package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.user.User;
import cz.muni.fi.pa165.model.dto.role.enums.RoleTypeEnum;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Role extends DomainObject {
    @ManyToOne
    @JoinColumn(name= "owner_id")
    protected User user;
    @Enumerated(EnumType.STRING)
    protected RoleTypeEnum roleType;
}
