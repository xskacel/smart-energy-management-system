package cz.muni.fi.pa165.core.company;

import cz.muni.fi.pa165.core.common.DomainFacade;

import cz.muni.fi.pa165.core.helpers.exceptions.EntityDeletionException;
import cz.muni.fi.pa165.core.helpers.exceptions.EntityConflictException;
import cz.muni.fi.pa165.model.dto.company.CompanyCreateDto;
import cz.muni.fi.pa165.model.dto.company.CompanyDto;
import cz.muni.fi.pa165.model.dto.company.CompanyUpdateDto;
import cz.muni.fi.pa165.model.dto.device.DeviceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CompanyFacade extends DomainFacade<Company, CompanyDto, CompanyCreateDto, CompanyUpdateDto> {
	private final CompanyService companyService; // For the "CompanyService" specific methods

	@Autowired
	public CompanyFacade(CompanyService companyService,
						 CompanyMapper companyMapper) {
		super(companyService, companyMapper);
		this.companyService = companyService;
	}

	@Override
	public CompanyDto deleteById(String id) throws EntityDeletionException {
		Company company = companyService.findById(id);
		if (!company.getEmployeeList().isEmpty() || !company.getHouseList().isEmpty()) {
			throw new EntityDeletionException("Company", new String[]{"employees", "houses"});
		}
		return mapper.toDto(this.companyService.deleteById(id));
	}
	@Override
	public CompanyDto create(CompanyCreateDto createDto) {
		if (companyService.findByName(createDto.getName()) != null) {
			throw new EntityConflictException("Company with given name already exists");
		}
		return super.create(createDto);
	}

	@Override
	public CompanyDto updateById(CompanyUpdateDto updateDto, String id) {
		if (companyService.findByName(updateDto.getName()) != null) {
			throw new EntityConflictException("Company with given name already exists");
		}
		return super.updateById(updateDto, id);
	}
}