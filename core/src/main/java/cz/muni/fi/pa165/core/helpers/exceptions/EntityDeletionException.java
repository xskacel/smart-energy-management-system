package cz.muni.fi.pa165.core.helpers.exceptions;

import org.apache.logging.log4j.message.Message;

public class EntityDeletionException extends Exception{
    private final String text = "cannot be deleted. It contains some of the following entities:";
    private final String message;
    public EntityDeletionException(String entityName, String[] entities) {
        this.message = entityName + " " + text + " " +  String.join(" ", entities);
    }
}
