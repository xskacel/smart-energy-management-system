package cz.muni.fi.pa165.core.user.roles;

import cz.muni.fi.pa165.model.dto.role.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper{
    HouseRole fromHouseDto(HouseRoleDto entity);
    CompanyRole fromCompanyDto(CompanyRoleDto entity);
    Role fromGeneralDto(RoleDto entity);

    HouseRoleDto toHouseDto(HouseRole entity);
    CompanyRoleDto toCompanyDto(CompanyRole entity);
    RoleDto toGeneralDto(Role entity);

    List<RoleDto> toGeneralListDto(List<Role> entities);
}
