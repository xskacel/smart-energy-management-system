package cz.muni.fi.pa165.core.user;

import cz.muni.fi.pa165.core.common.DomainObject;
import cz.muni.fi.pa165.core.house.House;
import cz.muni.fi.pa165.core.user.roles.Role;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain_user")
public class User extends DomainObject {

  @Column(unique = true, nullable = false)
  private String username;

  @Enumerated(EnumType.STRING)
  private UserType userType;

  private String password;

  private String email;

  private String firstName;

  private String lastName;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
  private List<Role> rolesList;
}
