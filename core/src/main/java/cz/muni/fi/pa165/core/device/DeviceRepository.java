package cz.muni.fi.pa165.core.device;

import cz.muni.fi.pa165.core.company.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String> {
	Optional<Device> findByName(String name);

}
