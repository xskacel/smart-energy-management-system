from locust import HttpUser, task, between

class SpringBootUser(HttpUser):
    wait_time = between(1, 3)

    @task
    def electricity_prices(self):
        self.client.get("/api/electricityprices")

    @task
    def electricity_prices_company(self):
        self.client.get("/api/electricityprices/avg/hightariff")